package com.hablax;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;


import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class HablaxCordovaPlugin extends CordovaPlugin {

    @Override
    public boolean execute(final String action, final JSONArray args, final CallbackContext command) throws JSONException {
        final Context context = this.cordova.getActivity();
        this.cordova.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (action.equals("onDidBecomeActive")) {

                    final BroadcastReceiver receiver = new BroadcastReceiver() {

                        @Override
                        public void onReceive(Context context, final Intent intent) {
                            final Bundle bundle = intent.getExtras(); // data if any
                            PluginResult dataResult = new PluginResult(PluginResult.Status.OK);
                            dataResult.setKeepCallback(true);
                            command.sendPluginResult(dataResult);
                        }
                    };

                    context.registerReceiver(receiver, new IntentFilter("onDidBecomeActive"));

                    PluginResult dataResult = new PluginResult(PluginResult.Status.NO_RESULT);
                    dataResult.setKeepCallback(true);
                    command.sendPluginResult(dataResult);

                }
            }
        });

        return true;
    }

}

